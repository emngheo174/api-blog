<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Program;
use App\Http\Requests\BlogRequest;
use App\Http\Resources\BlogResource;
use App\Responsitories\BlogRepository;
use PhpParser\Node\Expr\FuncCall;

class BlogController extends Controller
{

    protected $repository;

    /**
     * Declare constructor with Program model
     * 
     * @param Program
     * @param $blog
     * @return object
     */
    public function __construct(Program $blog)
    {
        $this->repository = new BlogRepository($blog);
    }
    
    /**
     * Get all records from Blog
     * 
     * @return array
     */
    public function index()
    {
        $blog = $this->repository->getAll();
        return response()->json(BlogResource::collection($blog));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function store(BlogRequest $request)
    {

        $blog = $this->repository->create($request->all());

        return response()->json( new BlogResource($blog));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return object
     */
    public function show($id)
    {
        $blog = $this->repository->find($id);
        if (is_null($blog)) {
            return response()->json('Data not found', 404);
        }
        return response()->json(new BlogResource($blog));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return object
     */
    public function update(BlogRequest $request, $id)
    {
        $data = $this->repository->update($request->all(), $id);

        return response()->json([
            'data' => $data
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return boolean
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return response()->json('Blog deleted successfully');
    }

    /**
     * Search the specified resource by name.
     *
     * @param  name
     * @return array 
     */
    public function search($name)
    {
        $blog = $this->repository->search($name);
        if (is_null($blog)) {
            return response()->json('Data not found', 404);
        }
        return response()->json(BlogResource::collection($blog));
    }
}
