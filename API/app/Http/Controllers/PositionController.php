<?php

namespace App\Http\Controllers;
use App\Http\Resources\PositionResources;
use App\Models\Position;
use App\Responsitories\PositionRepository;
use Illuminate\Http\Request;

class PositionController extends Controller
{
    protected $model;

    /**
     * Declare constructor with Position model
     * 
     * @param Position
     * @param $position
     * @return object
     */
    public function __construct(Position $position)
    {
        $this->model = new PositionRepository($position);
    }

    /**
     * Get all records from Position table
     * 
     * @return array
     */
    public function getAll()
    {
        $positions = $this->model->getAll();
        return response()->json( PositionResources::collection($positions));
    }
}
