<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Program extends Model
{

    use HasFactory;
    protected $fillable = ['title', 'desc', 'detail', 'position_id', 'public', 'category_id', 'image'];

    public function category()
    {
        return $this->belongsTo('categories', 'category_id');
    }

    public function positions()
    {
        return $this->belongsToMany('positions', 'blog_positions');
    }
}
