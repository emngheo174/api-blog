<?php

namespace App\Responsitories;

use App\Models\Program;
use App\Responsitories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository implements RepositoryInterface
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model =$model;
    }
    /**
     * Get one
     * 
     * @param $id
     * @return mixed
     */
    public function getAll()
    {
        return $this->model->all();
    }
    /** 
     * Find by id
     * 
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        $result = $this->model->find($id);

        return $result;
    }
    /**
     * Create 
     * @param $id
     * @param array attributes
     * @return object
     */
    public function create($attributes = [])
    {
        return $this->model->create($attributes);
    }
    /**
     * Update by id
     * 
     * @param $id
     * @return mixed
     */
    public function update( $data, $id)
    {
        
        $result = $this->model->find($id);
       
           return $result->update($data);
    }
    /**
     * Delete by id
     * 
     * @param $id
     * @return boolean
     */
    public function delete($id)
    {
        $result = $this->find($id);
        if ($result) {
            $result->delete();

            return true;
        }
        return false;
    }

}